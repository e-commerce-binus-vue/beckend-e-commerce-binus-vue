package com.example.backend.beckend.model;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "image_product")
    private String image;

    @Column(name = "nama_product")
    private String namaProduct;

    @Column(name = "price")
    private String price;

    @Column(name = "deskripsi")
    private String deskripsi;

    public Product() {
    }

    public Product(String image, String namaProduct, String price, String deskripsi, boolean b) {
        this.image = image;
        this.namaProduct = namaProduct;
        this.price = price;
        this.deskripsi = deskripsi;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNamaProduct() {
        return namaProduct;
    }

    public void setNamaProduct(String namaProduct) {
        this.namaProduct = namaProduct;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
