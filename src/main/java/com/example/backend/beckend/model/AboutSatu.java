package com.example.backend.beckend.model;

import javax.persistence.*;

@Entity
@Table(name = "aboutsatu")
public class AboutSatu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "kategori")
    private String kategori;

    @Column(name = "deskripsi")
    private String deskripsi;

    public AboutSatu() {
    }

    public AboutSatu(String kategori, String deskripsi, boolean b) {
        this.kategori = kategori;
        this.deskripsi = deskripsi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
