package com.example.backend.beckend.model;

import javax.persistence.*;

@Entity
@Table(name = "blog")
public class Blog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tanggal")
    private String tanggal;

    @Column(name = "image")
    private String image;

    @Column(name = "judul")
    private String judul;

    public Blog() {
    }

    public Blog(String tanggal, String image, String judul, boolean b) {
        this.tanggal = tanggal;
        this.image = image;
        this.judul = judul;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }
}
