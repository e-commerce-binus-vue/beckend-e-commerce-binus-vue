package com.example.backend.beckend.model;

import javax.persistence.*;

@Entity
@Table(name = "contact")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "lokasi")
    private String lokasi;

    @Column(name = "no_telp")
    private String noTelp;

    @Column(name = "email")
    private String email;

    @Column(name = "open_hours")
    private String openHours;

    @Column(name = "map_lokasi")
    private String mapLokasi;

    public Contact() {
    }

    public Contact(String lokasi, String noTelp, String email, String openHours, String mapLokasi, boolean b) {
        this.lokasi = lokasi;
        this.noTelp = noTelp;
        this.email = email;
        this.openHours = openHours;
        this.mapLokasi = mapLokasi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOpenHours() {
        return openHours;
    }

    public void setOpenHours(String openHours) {
        this.openHours = openHours;
    }

    public String getMapLokasi() {
        return mapLokasi;
    }

    public void setMapLokasi(String mapLokasi) {
        this.mapLokasi = mapLokasi;
    }
}
