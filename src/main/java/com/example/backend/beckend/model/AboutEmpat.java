package com.example.backend.beckend.model;

import javax.persistence.*;

@Entity
@Table(name = "phylosophy")
public class AboutEmpat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image")
    private String image;

    @Column(name = "tema")
    private String tema;

    @Column(name = "deskripsi")
    private String deskripsi;

    public AboutEmpat() {
    }

    public AboutEmpat(String image, String tema, String deskripsi, boolean b) {
        this.image = image;
        this.tema = tema;
        this.deskripsi = deskripsi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
