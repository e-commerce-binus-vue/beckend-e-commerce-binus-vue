package com.example.backend.beckend.model;

import javax.persistence.*;

@Entity
@Table(name = "recent_post")
public class RecentPost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image")
    private String image;

    @Column(name = "tanggal")
    private String tanggal;

    @Column(name = "judul")
    private String judul;

    public RecentPost() {
    }

    public RecentPost(String image, String tanggal, String judul, boolean b) {
        this.image = image;
        this.tanggal = tanggal;
        this.judul = judul;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }
}
