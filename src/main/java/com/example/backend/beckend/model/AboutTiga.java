package com.example.backend.beckend.model;

import javax.persistence.*;

@Entity
@Table(name = "The_best_team_available")
public class AboutTiga {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image")
    private String image;

    @Column(name = "nama_orang")
    private String namaOrang;

    @Column(name = "nama_team")
    private String namaTeam;

    public AboutTiga() {
    }

    public AboutTiga(String image, String namaOrang, String namaTeam, boolean b) {
        this.image = image;
        this.namaOrang = namaOrang;
        this.namaTeam = namaTeam;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNamaOrang() {
        return namaOrang;
    }

    public void setNamaOrang(String namaOrang) {
        this.namaOrang = namaOrang;
    }

    public String getNamaTeam() {
        return namaTeam;
    }

    public void setNamaTeam(String namaTeam) {
        this.namaTeam = namaTeam;
    }
}
