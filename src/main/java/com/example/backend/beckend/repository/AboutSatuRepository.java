package com.example.backend.beckend.repository;

import com.example.backend.beckend.model.AboutSatu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AboutSatuRepository extends JpaRepository<AboutSatu, Long> {

    List<AboutSatu> findByKategoriContaining(String kategori);
}
