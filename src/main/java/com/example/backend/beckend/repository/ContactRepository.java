package com.example.backend.beckend.repository;

import com.example.backend.beckend.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Long> {

    List<Contact> findByLokasiContaining(String lokasi);

}
