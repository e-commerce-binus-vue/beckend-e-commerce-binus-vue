package com.example.backend.beckend.repository;

import com.example.backend.beckend.model.AboutDua;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AboutDuaRepository extends JpaRepository<AboutDua, Long> {

    List<AboutDua> findByNamaOrangContaining(String namaOrang);
}