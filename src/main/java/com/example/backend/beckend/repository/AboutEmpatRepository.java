package com.example.backend.beckend.repository;

import com.example.backend.beckend.model.AboutEmpat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AboutEmpatRepository extends JpaRepository<AboutEmpat, Long> {

    List<AboutEmpat> findByTemaContaining(String tema);

}
