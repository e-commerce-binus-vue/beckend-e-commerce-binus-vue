package com.example.backend.beckend.repository;

import com.example.backend.beckend.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findByNamaProductContaining(String namaProduct);

}