package com.example.backend.beckend.repository;

import com.example.backend.beckend.model.RecentPost;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecentPostRepository extends JpaRepository<RecentPost, Long> {

    List<RecentPost> findByJudulContaining(String judul);

}
