package com.example.backend.beckend.repository;

import com.example.backend.beckend.model.Blog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BlogRepository extends JpaRepository<Blog, Long> {

    List<Blog> findByJudulContaining(String judul);

}
