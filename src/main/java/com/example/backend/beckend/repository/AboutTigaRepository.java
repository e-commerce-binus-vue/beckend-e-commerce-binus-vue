package com.example.backend.beckend.repository;

import com.example.backend.beckend.model.AboutTiga;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AboutTigaRepository extends JpaRepository<AboutTiga, Long> {

    List<AboutTiga> findByNamaOrangContaining(String namaOrang);
}
