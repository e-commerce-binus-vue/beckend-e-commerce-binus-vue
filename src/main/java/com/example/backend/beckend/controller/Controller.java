package com.example.backend.beckend.controller;

import com.example.backend.beckend.model.*;
import com.example.backend.beckend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class Controller {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    BlogRepository blogRepository;
    @Autowired
    AboutSatuRepository aboutSatuRepository;
    @Autowired
    AboutDuaRepository aboutDuaRepository;
    @Autowired
    AboutTigaRepository aboutTigaRepository;
    @Autowired
    AboutEmpatRepository aboutEmpatRepository;
    @Autowired
    RecentPostRepository recentPostRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ContactRepository contactRepository;

//    CATEGORY
    @GetMapping("/contact")
    public ResponseEntity<List<Contact>> getAlContact(@RequestParam(required = false) String lokasi) {
        try {
            List<Contact> contact = new ArrayList<>();
            if (lokasi == null)
                contactRepository.findAll().forEach(contact::add);
            else
                contactRepository.findByLokasiContaining(lokasi).forEach(contact::add);
            if (contact.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(contact, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/contact")
    public ResponseEntity<Contact> createContact(@RequestBody Contact contact) {
        try {
            Contact _contact = contactRepository
                    .save(new Contact(contact.getLokasi(), contact.getNoTelp(), contact.getEmail(), contact.getOpenHours(), contact.getMapLokasi(), false));
            return new ResponseEntity<>(_contact, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/contact/{id}")
    public ResponseEntity<Contact> updateContact(@PathVariable("id") long id, @RequestBody Contact contact) {
        Optional<Contact> contactData = contactRepository.findById(id);
        if (contactData.isPresent()) {
            Contact _contact = contactData.get();
            _contact.setLokasi(contact.getLokasi());
            _contact.setNoTelp(contact.getNoTelp());
            _contact.setEmail(contact.getEmail());
            _contact.setOpenHours(contact.getOpenHours());
            _contact.setMapLokasi(contact.getMapLokasi());
            return new ResponseEntity<>(contactRepository.save(_contact), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/contact/{id}")
    public ResponseEntity<Contact> getContactById(@PathVariable("id") long id) {
        Optional<Contact> contactData = contactRepository.findById(id);
        if (contactData.isPresent()) {
            return new ResponseEntity<>(contactData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

//    CATEGORY
    @GetMapping("/category")
    public ResponseEntity<List<Category>> getAlCategory(@RequestParam(required = false) String name) {
        try {
            List<Category> category = new ArrayList<>();
            if (name == null)
                categoryRepository.findAll().forEach(category::add);
            else
                categoryRepository.findByNameContaining(name).forEach(category::add);
            if (category.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(category, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/category")
    public ResponseEntity<Category> createCategory(@RequestBody Category category) {
        try {
            Category _category = categoryRepository
                    .save(new Category(category.getName(), false));
            return new ResponseEntity<>(_category, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//  RECENT POST
    @GetMapping("/recent")
    public ResponseEntity<List<RecentPost>> getAllRecent(@RequestParam(required = false) String judul) {
        try {
            List<RecentPost> recentPosts = new ArrayList<>();
            if (judul == null)
                recentPostRepository.findAll().forEach(recentPosts::add);
            else
                recentPostRepository.findByJudulContaining(judul).forEach(recentPosts::add);
            if (recentPosts.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(recentPosts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/recent")
    public ResponseEntity<RecentPost> createRecent(@RequestBody RecentPost recentPost) {
        try {
            RecentPost _recent = recentPostRepository
                    .save(new RecentPost(recentPost.getImage(), recentPost.getTanggal(), recentPost.getJudul(), false));
            return new ResponseEntity<>(_recent, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    ABOUT EMPAT
    @GetMapping("/aboutempat")
    public ResponseEntity<List<AboutEmpat>> getAllAboutEmpat(@RequestParam(required = false) String tema) {
        try {
            List<AboutEmpat> aboutempats = new ArrayList<AboutEmpat>();
            if (tema == null)
                aboutEmpatRepository.findAll().forEach(aboutempats::add);
            else
                aboutEmpatRepository.findByTemaContaining(tema).forEach(aboutempats::add);
            if (aboutempats.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(aboutempats, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/aboutempat/{id}")
    public ResponseEntity<AboutEmpat> getAboutEmpatById(@PathVariable("id") long id) {
        Optional<AboutEmpat> aboutEmpatData = aboutEmpatRepository.findById(id);
        if (aboutEmpatData.isPresent()) {
            return new ResponseEntity<>(aboutEmpatData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/aboutempat")
    public ResponseEntity<AboutEmpat> createAboutEmpat(@RequestBody AboutEmpat aboutEmpat) {
        try {
            AboutEmpat _aboutEmpat = aboutEmpatRepository
                    .save(new AboutEmpat(aboutEmpat.getImage(), aboutEmpat.getTema(), aboutEmpat.getDeskripsi(), false));
            return new ResponseEntity<>(_aboutEmpat, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/aboutempat/{id}")
    public ResponseEntity<AboutEmpat> updateAboutEmpat(@PathVariable("id") long id, @RequestBody AboutEmpat aboutEmpat) {
        Optional<AboutEmpat> aboutEmpatData = aboutEmpatRepository.findById(id);
        if (aboutEmpatData.isPresent()) {
            AboutEmpat _aboutEmpat = aboutEmpatData.get();
            _aboutEmpat.setImage(aboutEmpat.getImage());
            _aboutEmpat.setTema(aboutEmpat.getTema());
            _aboutEmpat.setDeskripsi(aboutEmpat.getDeskripsi());
            return new ResponseEntity<>(aboutEmpatRepository.save(_aboutEmpat), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/aboutempat/{id}")
    public ResponseEntity<HttpStatus> deleteAboutEmpat(@PathVariable("id") long id) {
        try {
            aboutEmpatRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    ABOUT TIGA
    @GetMapping("/abouttiga")
    public ResponseEntity<List<AboutTiga>> getAllAboutTiga(@RequestParam(required = false) String namaOrang) {
        try {
            List<AboutTiga> abouttigas = new ArrayList<AboutTiga>();
            if (namaOrang == null)
                aboutTigaRepository.findAll().forEach(abouttigas::add);
            else
                aboutTigaRepository.findByNamaOrangContaining(namaOrang).forEach(abouttigas::add);
            if (abouttigas.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(abouttigas, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/abouttiga/{id}")
    public ResponseEntity<AboutTiga> getAboutTigaById(@PathVariable("id") long id) {
        Optional<AboutTiga> aboutTigaData = aboutTigaRepository.findById(id);
        if (aboutTigaData.isPresent()) {
            return new ResponseEntity<>(aboutTigaData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/abouttiga")
    public ResponseEntity<AboutTiga> createAboutTiga(@RequestBody AboutTiga aboutTiga) {
        try {
            AboutTiga _aboutTiga = aboutTigaRepository
                    .save(new AboutTiga(aboutTiga.getImage(), aboutTiga.getNamaOrang(), aboutTiga.getNamaTeam(), false));
            return new ResponseEntity<>(_aboutTiga, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/abouttiga/{id}")
    public ResponseEntity<AboutTiga> updateAboutTiga(@PathVariable("id") long id, @RequestBody AboutTiga aboutTiga) {
        Optional<AboutTiga> aboutTigaData = aboutTigaRepository.findById(id);
        if (aboutTigaData.isPresent()) {
            AboutTiga _aboutTiga = aboutTigaData.get();
            _aboutTiga.setImage(aboutTiga.getImage());
            _aboutTiga.setNamaOrang(aboutTiga.getNamaOrang());
            _aboutTiga.setNamaTeam(aboutTiga.getNamaTeam());
            return new ResponseEntity<>(aboutTigaRepository.save(_aboutTiga), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/abouttiga/{id}")
    public ResponseEntity<HttpStatus> deleteAboutTuga(@PathVariable("id") long id) {
        try {
            aboutTigaRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    ABOUT DUA
    @GetMapping("/aboutdua")
    public ResponseEntity<List<AboutDua>> getAllAboutDua(@RequestParam(required = false) String namaOrang) {
        try {
            List<AboutDua> abouts = new ArrayList<AboutDua>();
            if (namaOrang == null)
                aboutDuaRepository.findAll().forEach(abouts::add);
            else
                aboutDuaRepository.findByNamaOrangContaining(namaOrang).forEach(abouts::add);
            if (abouts.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(abouts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/aboutdua/{id}")
    public ResponseEntity<AboutDua> getAboutDuaById(@PathVariable("id") long id) {
        Optional<AboutDua> aboutDuaData = aboutDuaRepository.findById(id);
        if (aboutDuaData.isPresent()) {
            return new ResponseEntity<>(aboutDuaData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/aboutdua")
    public ResponseEntity<AboutDua> createAboutDua(@RequestBody AboutDua aboutDua) {
        try {
            AboutDua _aboutDua = aboutDuaRepository
                    .save(new AboutDua(aboutDua.getImage(), aboutDua.getNamaOrang(), aboutDua.getNamaTeam(), false));
            return new ResponseEntity<>(_aboutDua, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/aboutdua/{id}")
    public ResponseEntity<AboutDua> updateAboutDua(@PathVariable("id") long id, @RequestBody AboutDua aboutDua) {
        Optional<AboutDua> aboutDuaData = aboutDuaRepository.findById(id);
        if (aboutDuaData.isPresent()) {
            AboutDua _aboutDua = aboutDuaData.get();
            _aboutDua.setImage(aboutDua.getImage());
            _aboutDua.setNamaOrang(aboutDua.getNamaOrang());
            _aboutDua.setNamaTeam(aboutDua.getNamaTeam());
            return new ResponseEntity<>(aboutDuaRepository.save(_aboutDua), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/aboutdua/{id}")
    public ResponseEntity<HttpStatus> deleteAboutDua(@PathVariable("id") long id) {
        try {
            aboutDuaRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    ABOUT SATU
    @GetMapping("/aboutsatu")
    public ResponseEntity<List<AboutSatu>> getAllAboutSatu(@RequestParam(required = false) String kategori) {
        try {
            List<AboutSatu> aboutSatus = new ArrayList<AboutSatu>();
            if (kategori == null)
                aboutSatuRepository.findAll().forEach(aboutSatus::add);
            else
                aboutSatuRepository.findByKategoriContaining(kategori).forEach(aboutSatus::add);
            if (aboutSatus.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(aboutSatus, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/aboutsatu/{id}")
    public ResponseEntity<AboutSatu> getAboutSatuById(@PathVariable("id") long id) {
        Optional<AboutSatu> aboutSatuData = aboutSatuRepository.findById(id);
        if (aboutSatuData.isPresent()) {
                return new ResponseEntity<>(aboutSatuData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/aboutsatu")
    public ResponseEntity<AboutSatu> createAboutSatu(@RequestBody AboutSatu aboutSatu) {
        try {
            AboutSatu _aboutSatu = aboutSatuRepository
                    .save(new AboutSatu(aboutSatu.getKategori(), aboutSatu.getDeskripsi(), false));
            return new ResponseEntity<>(_aboutSatu, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/aboutsatu/{id}")
    public ResponseEntity<AboutSatu> updateAboutSatu(@PathVariable("id") long id, @RequestBody AboutSatu aboutSatu) {
        Optional<AboutSatu> aboutSatuData = aboutSatuRepository.findById(id);
        if (aboutSatuData.isPresent()) {
            AboutSatu _aboutSatu = aboutSatuData.get();
            _aboutSatu.setKategori(aboutSatu.getKategori());
            _aboutSatu.setDeskripsi(aboutSatu.getDeskripsi());
            return new ResponseEntity<>(aboutSatuRepository.save(_aboutSatu), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/aboutsatu/{id}")
    public ResponseEntity<HttpStatus> deleteAboutSatu(@PathVariable("id") long id) {
        try {
            aboutSatuRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    BLOG
    @GetMapping("/blog")
    public ResponseEntity<List<Blog>> getAllBlog(@RequestParam(required = false) String judul) {
        try {
            List<Blog> blog = new ArrayList<>();
            if (judul == null)
                blogRepository.findAll().forEach(blog::add);
            else
                blogRepository.findByJudulContaining(judul).forEach(blog::add);
            if (blog.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(blog, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/blog/{id}")
    public ResponseEntity<Blog> getBlogById(@PathVariable("id") long id) {
        Optional<Blog> blogData = blogRepository.findById(id);
        if (blogData.isPresent()) {
            return new ResponseEntity<>(blogData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/blog")
    public ResponseEntity<Blog> createBlog(@RequestBody Blog blog) {
        try {
            Blog _blog = blogRepository
                    .save(new Blog(blog.getTanggal(), blog.getImage(), blog.getJudul(), false));
            return new ResponseEntity<>(_blog, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/blog/{id}")
    public ResponseEntity<Blog> updateBlog(@PathVariable("id") long id, @RequestBody Blog blog) {
        Optional<Blog> blogData = blogRepository.findById(id);
        if (blogData.isPresent()) {
            Blog _blog = blogData.get();
            _blog.setTanggal(blog.getTanggal());
            _blog.setImage(blog.getImage());
            _blog.setJudul(blog.getJudul());
            return new ResponseEntity<>(blogRepository.save(_blog), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/blog/{id}")
    public ResponseEntity<HttpStatus> deleteBlog(@PathVariable("id") long id) {
        try {
            blogRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    PRODUCT
    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProducts(@RequestParam(required = false) String namaProduct) {
        try {
            List<Product> products = new ArrayList<Product>();
            if (namaProduct == null)
                productRepository.findAll().forEach(products::add);
            else
                productRepository.findByNamaProductContaining(namaProduct).forEach(products::add);
            if (products.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") long id) {
        Optional<Product> productData = productRepository.findById(id);
        if (productData.isPresent()) {
            return new ResponseEntity<>(productData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        try {
            Product _product = productRepository
                    .save(new Product(product.getImage(), product.getNamaProduct(), product.getPrice(), product.getDeskripsi(), false));
            return new ResponseEntity<>(_product, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable("id") long id, @RequestBody Product product) {
        Optional<Product> productData = productRepository.findById(id);
        if (productData.isPresent()) {
            Product _product = productData.get();
            _product.setNamaProduct(product.getNamaProduct());
            _product.setImage(product.getImage());
            _product.setPrice(product.getPrice());
            _product.setDeskripsi(product.getDeskripsi());
            return new ResponseEntity<>(productRepository.save(_product), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<HttpStatus> deleteProduct(@PathVariable("id") long id) {
        try {
            productRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
